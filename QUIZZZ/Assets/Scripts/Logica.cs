﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Quiz
{
    public string texto;
}

public class ClassResposta : Quiz
{
    public bool ecorreta;
    
    public ClassResposta(string a, bool b)
    {
        this.texto = a;
        this.ecorreta = b;
    }
}

public class ClassPergunta : Quiz
{ // CLASS PERGUNTAS
    public int pontos;
    public ClassResposta[] respostas;

    // METODO É PARA CRIAR AS PERGUNTAS
    public ClassPergunta(string t, int p)
    {
        this.texto = t;
        this.pontos = p;
    }

    // METODO PARA ADD RESPOSTA
    public void AddResposta(string a, bool b)
    {
        if (respostas == null)
        {
            respostas = new ClassResposta[4];
        }

        for (int i = 0; i < respostas.Length; i++)
        {
            if (respostas[i] == null)
            {
                respostas[i] = new ClassResposta(a, b);
                break;
            }
        }
    }

    // LISTA ou EXIBIR as respostas
    public void ListarRespostas()
    {
        if (respostas != null && respostas.Length > 0)
        {
            foreach (ClassResposta r in respostas)
            {
                Debug.Log(r);
            }
        }
    }

    // Checar resposta correta
    public bool ChecarRespostaCorreta(int pos)
    {
        if (respostas != null && respostas.Length > pos && pos >= 0)
        {
            return respostas[pos].ecorreta;
        }
        else
        {
            return false;
        }
    }

} // FIM CLASS PERGUNTAS


// CLASS LÓGICA - PRINCIPAL
public class Logica : MonoBehaviour
{ // INICIO CLASS LOGICA

    public Text tituloPergunta;
    public Button[] respostaBtn = new Button[4];
    public List<ClassPergunta> PerguntasLista = new List<ClassPergunta>();

    private ClassPergunta perguntaAtual;

    public GameObject PainelCorreto;
    public GameObject PainelErrado;
    public GameObject PainelFinal;
    private int contagemPerguntas = 0;
    private float tempo;
    private bool carregarProximaPergunta;
    private int pontos = 0;


    void Start()
    { // METODO START

        ClassPergunta p1 = new ClassPergunta("1.Qual um dos piores alimentos para à saúde? ?", 5);
        p1.AddResposta("Verduras",false);
        p1.AddResposta("Sorvete", true);
        p1.AddResposta("queijo", false);
        p1.AddResposta("nenhuma das alternativas", false);

        ClassPergunta p2 = new ClassPergunta("2.Quais os 5 alimentos mais saudáveis?", 3);
        p2.AddResposta("Alimentos com sal.", false);
        p2.AddResposta("alimentos com açucar.", false);
        p2.AddResposta("Chocolate negro.", true);
        p2.AddResposta("nenhuma das alternativas.", false);

        ClassPergunta p3 = new ClassPergunta("3.	Quais alimentos aumentam o nível de açúcar no sangue?", 6);
        p3.AddResposta("Produtos sem açúcar.", true);
        p3.AddResposta("Produtos com açúcar.", false);
        p3.AddResposta("Salgadinhos.", false);
        p3.AddResposta("nenhuma das alternativas.", false);

        ClassPergunta p4 = new ClassPergunta("4.Quais um dos melhores alimentos para quem faz dieta? ", 2);
        p4.AddResposta("Alimentos sem açucar.", false);
        p4.AddResposta("gorduras", false);
        p4.AddResposta("Vinagre de maça.", true);
        p4.AddResposta("nenhuma das alternativas..", false);

        ClassPergunta p5 = new ClassPergunta("5.	Qual o principal alimento desencadeante de diabete? ", 5);
        p5.AddResposta("alimentos com açucar.", false);
        p5.AddResposta("Pão.", true);
        p5.AddResposta("Barra de cereal.", false);
        p5.AddResposta("nenhuma das alternativas.", false);

        ClassPergunta p6 = new ClassPergunta("6.	Qual das alternativas é mito?.", 5);
        p6.AddResposta("A laranja é rica em vitamina c.", false);
        p6.AddResposta(" sal  é um mineral formado  por cloreto de sódio .", false);
        p6.AddResposta("Nenhuma das alternativas.", false);
        p6.AddResposta("Comer ovo faz o colesterol disparar. .", true);

        ClassPergunta p7 = new ClassPergunta("7.Em relação ao índice glicêmico: Qual alimento a seguir deve ser consumido em menor quantidade?", 5);
        p7.AddResposta("Alimetos com açucar.", false);
        p7.AddResposta("Alimentos com sal.", false);
        p7.AddResposta("Verduras", true);
        p7.AddResposta("Nenhuma das alternativas.", false);

        ClassPergunta p8 = new ClassPergunta("8.	Quais alimentos o hipertenso deve evitar e por quê?", 5);
        p8.AddResposta("Alimetos ricos em açucar", false);
        p8.AddResposta("frutas", false);
        p8.AddResposta("Alimentos ricos em sódio", true);
        p8.AddResposta("Nenhuma das alternativas", false);

        ClassPergunta p9 = new ClassPergunta("9.	Quais os melhores alimentos para fortalecer a pele?", 5);
        p9.AddResposta("Alimentos ricos em vitamina D", false);
        p9.AddResposta("Alimentos ricos em vitamina A = ", false);
        p9.AddResposta("Nenhuma das alternativas", false);
        p9.AddResposta("Alimentos ricos em vitamina C, ", true);

        ClassPergunta p10 = new ClassPergunta("10. A carne de soja apresenta o mesmo valor nutricional da carne bovina?", 5);
        p10.AddResposta("Nenhuma das alternativas", false);
        p10.AddResposta("quase o mesmo", false);
        p10.AddResposta("Sim", false);
        p10.AddResposta("não", true);



        PerguntasLista.Add(p1);
        PerguntasLista.Add(p2);
        PerguntasLista.Add(p3);
        PerguntasLista.Add(p4);
        PerguntasLista.Add(p5);
        PerguntasLista.Add(p6);
        PerguntasLista.Add(p7);
        PerguntasLista.Add(p8);
        PerguntasLista.Add(p9);
        PerguntasLista.Add(p10);

        perguntaAtual = p1;

        ExibirPerguntasNoQuiz();


    } // FIM METODO START

    void Update()
    {
        if (carregarProximaPergunta == true)
        {
            tempo += Time.deltaTime;
            if (tempo > 3)
            {
                tempo = 0;
                carregarProximaPergunta = false;
                ProximaPergunta();
            }
        }
    }


    private void ExibirPerguntasNoQuiz()
    {
        tituloPergunta.text = perguntaAtual.texto;
        respostaBtn[0].GetComponentInChildren<Text>().text = perguntaAtual.respostas[0].texto;
        respostaBtn[1].GetComponentInChildren<Text>().text = perguntaAtual.respostas[1].texto;
        respostaBtn[2].GetComponentInChildren<Text>().text = perguntaAtual.respostas[2].texto;
        respostaBtn[3].GetComponentInChildren<Text>().text = perguntaAtual.respostas[3].texto;
    }

    public void ProximaPergunta()
    {
        contagemPerguntas++;
        if (contagemPerguntas < PerguntasLista.Count)
        {
            perguntaAtual = PerguntasLista[contagemPerguntas];
            ExibirPerguntasNoQuiz();
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
        }
        else
        {
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
            PainelFinal.SetActive(true);
            PainelFinal.transform.GetComponentInChildren<Text>().text = "Pontos: " + pontos.ToString();
        }
    }

    public void ClickResposta(int pos)
    {
        if (perguntaAtual.respostas[pos].ecorreta)
        {
            //Debug.Log("Resposta certa!!!");
            PainelCorreto.SetActive(true);
            pontos += perguntaAtual.pontos;
                
        }
        else
        {
            //Debug.Log("Resposta errada!!!");
            PainelErrado.SetActive(true);
        }

        carregarProximaPergunta = true;
    }

    public void IniciarQuiz()
    {
        SceneManager.LoadScene("Quiz");
    }

    public void MenuInicial()
    {
        SceneManager.LoadScene("MenuInicial");
    }

    public void Credito()
    {
        SceneManager.LoadScene("Credito");
    }

    public void SairJogo()
    {
        Application.Quit();
    }




}// FIM CLASS LOGICA
